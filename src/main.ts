import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify.js';
import ToastsPlugin from "./plugins/notifications";
Vue.config.productionTip = false
export  const Bus = new Vue()
Vue.use(ToastsPlugin);
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
