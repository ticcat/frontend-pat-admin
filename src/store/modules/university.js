
export const university = {
    namespaced:true,
    state: {
        facultades:[],
        especialidades:[]
    },
    mutations: {
        setFacultades(state, facultades) {
            state.facultades=facultades;
        },
        setEspecialidades(state, especialidades) {
            state.especialidades=especialidades;
        }
    },
    actions: {
        async getFacultades({commit}){
            const response = await fetch( 'https://ccatapp.herokuapp.com/api/facultades',{method: 'GET'});
            if(!response.ok){
                throw response;
            }else{
                if (response) commit('setFacultades', await response.json());
            }

        },
        async getEspecialidades({commit},facultad){
            commit("setEspecialidades", [])
            const response = await fetch( 'https://ccatapp.herokuapp.com/api/especialidades?facultad='+facultad ,{method: 'GET'});

            if(!response.ok){
                throw response;
            }else{
                if (response)commit("setEspecialidades", await response.json());
            }

        }



    }
};
