export const polls = {
    namespaced:true,
    state: {
        polls:[]

    },
    mutations: {
        updateAsambleas(state,asambleas){
            state.polls=asambleas
        }
    },
    actions: {
        async pullPolls({commit},option={pageNumber:0,pageSize:15}) {
            let response = await fetch('/api/polls?page='+option.pageNumber+"&size="+(option.pageSize||15), {
                method: "GET",
            })
            const data = await  response.json();
            commit("updateAsambleas",data)
        },
        async castVote(context, voteData) {
            let response = await fetch('/api/polls/'+ voteData.pollId + "/votes", {
                credentials: "include",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: 'POST',
                body: JSON.stringify(voteData)
            });
            context.dispatch("pullPolls")
            let data = await response.json()
            console.log(data)
        },
        async newPoll(context,asamblea){
            let response = await fetch('/api/polls', {
                credentials: "include",
                headers: {
                    "Content-Type": "application/json; charset=UTF-8"
                },
                method: 'POST',
                body: JSON.stringify(asamblea)
            });
            context.dispatch("pullPolls")
            if(!response.ok)
                throw await response.json()

            let data= await response.json()

        }
    }
};
