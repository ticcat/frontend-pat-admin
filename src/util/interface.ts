import _Vue from "vue";
import ToastContainer from "../components/VtToastContainer.vue";
import {
    PluginOptions,
} from "./types";

import { getId, isUndefined } from "./util";

const ToastInterface = (Vue: typeof _Vue, globalOptions?: PluginOptions) => {
    const containerComponent = new (Vue.extend(ToastContainer))({
        el: document.createElement("div"),
        propsData: globalOptions
    });
    const onMounted = globalOptions?.onMounted;
    if (!isUndefined(onMounted)) {
        onMounted(containerComponent);
    }

};

export default ToastInterface;
