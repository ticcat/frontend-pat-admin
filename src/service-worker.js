// custom service-worker.js
if (workbox) {
    // adjust log level for displaying workbox logs
    workbox.setConfig({
        debug: true
    });

    // apply precaching. In the built version, the precacheManifest will
    // be imported using importScripts (as is workbox itself) and we can
    // precache this. This is all we need for precaching
    workbox.precaching.precacheAndRoute(self.__precacheManifest);

    // Make sure to return a specific response for all navigation requests.
    // Since we have a SPA here, this should be index.html always.
    // https://stackoverflow.com/questions/49963982/vue-router-history-mode-with-pwa-in-offline-mode
    workbox.routing.registerNavigationRoute('/index.html');

    // Setup cache strategy for Google Fonts. They consist of two parts, a static one
    // coming from fonts.gstatic.com (strategy CacheFirst) and a more ferquently updated on
    // from fonts.googleapis.com. Hence, split in two registerroutes
    workbox.routing.registerRoute(
        /^https:\/\/fonts\.googleapis\.com/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'google-fonts-stylesheets',
        })
    );

    workbox.routing.registerRoute(
        /^https:\/\/fonts\.gstatic\.com/,
        new workbox.strategies.CacheFirst({
            cacheName: 'google-fonts-webfonts',
            plugins: [
                new workbox.cacheableResponse.Plugin({
                    statuses: [0, 200],
                }),
                new workbox.expiration.Plugin({
                    maxAgeSeconds: 60 * 60 * 24 * 365,
                    maxEntries: 30,
                }),
            ],
        })
    );

    workbox.routing.registerRoute(
        /^https:\/\/stackpath\.bootstrapcdn\.com/,
        new workbox.strategies.StaleWhileRevalidate({
            cacheName: 'fontawesome',
        })
    );


    // if you send a push notification from server, it might be instant or delay up 10 minutes
// https://developer.mozilla.org/en-US/docs/Web/API/PushEvent
    self.addEventListener('push', function(event) {
        console.log('[Service Worker] Push Received.');
        // push notification can send event.data.json() as well
        console.log(`[Service Worker] Push had this data: "${event.data.json()}"`);
          let data =  event.data.json();
        // https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/showNotification
        const title = data.title;
        const options = {
            body: data.body,
            icon: '/img/icons/android-chrome-512x512.png',
            badge: '/img/plint-badge-96x96.png',
            data: {
                url: '/watch'
            },
            tag: 'alert'
        };

        event.waitUntil(self.registration.showNotification(title, options));
    });


    self.addEventListener('notificationclick', function(event) {
        // can handle different type of notification based on event.notification.tag
        console.log(`[Service Worker] Notification click Received: ${event.notification.tag}`);

        event.notification.close();

        // Modify code from https://developer.mozilla.org/en-US/docs/Web/API/WindowClient/focus
        // find existing "/notification" window to focus on, or open a new one if not available
        event.waitUntil(clients.matchAll({
            type: "window"
        }).then(function(clientList) {
            const client = clientList.find(function(c) {
                new URL(c.url).pathname === '/notification'
            });
            if (client !== undefined) {
                return client.focus();
            }

            return clients.openWindow('/notification');
        }));
    });

    // This code listens for the user's confirmation to update the app.
    self.addEventListener('message', (e) => {
        if (!e.data) {
            return;
        }

        switch (e.data) {
            case 'skipWaiting':
                self.skipWaiting();
                break;
            default:
                // NOOP
                break;
        }
    })


}

