import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'home',
      component: () =>import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
      path: '/',
      name: 'loginAndRegister',
      component: () =>import(/* webpackChunkName: "loginRegister" */ '../views/LoginAndRegister.vue'),
      meta: {
        guest: true
      },
      children: [
      {

        path: '/login',
        name: 'login',
        component: () =>import(/* webpackChunkName: "login" */ '../components/login/Login.vue'),
        meta: {
          guest: true
        },

      },
      {
        path: '/signup',
        name: 'signup',
        component: () => import(/* webpackChunkName: "signup" */ '../components/register/Register.vue'),
        meta: {
          guest: true
        }
      },
    ]
  },
    {
      path: '/resetPassword',
      name: 'resetPassword',
      component: () => import(/* webpackChunkName: "resetPassword" */ '../views/ResetPassword.vue'),
      meta: {
        guest: true
      }
    },    {
      path: '/updatePassword',
      name: 'updatedPassword',
      component: () => import(/* webpackChunkName: "updatePassword" */ '../views/updatePassword.vue'),
      props: (route) => ({ token: route.query.token }),
      meta: {
        guest: true
      }
    },
    {
      path: '/asambleas',
      name: 'Asambleas',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/Asambleas.vue'),
      meta: {
        requiresAuth: true
      }
    },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
    {
      path: '/signup',
      name: 'signup',
      component: () => import(/* webpackChunkName: "signup" */ '../components/register/Register.vue'),
      meta: {
        guest: true
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import(/* webpackChunkName: "dashboard" */ '../views/DashBoard.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/account',
      name: 'account',
      component: () => import(/* webpackChunkName: "dashboard" */ '../views/Account.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: () => import(/* webpackChunkName: "admin" */ '../views/Admin.vue'),
      meta: {
        requiresAuth: true,
        isAdmin : true
      }
    },
    {
      path: '/admin/asamblea',
      name: 'adminAsamblea',
      component: () =>import(/* webpackChunkName: "adminAsambleas" */ '../views/admin/Asambleas.vue'),
      meta: {
        requiresAuth: true,
        isAdmin : true
      }
    },
    //   //Manejo de error, añadir path's en la parte superior
    // {  path: '*',
    //   redirect: '/404'
    // },
    // {
    //   path: '/404',
    //   name: '404',
    //   component: () => import(/* webpackChunkName: "admin" */ '../views/error.vue'),
    // }

]



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
import store from "../store/index.js"
import {auth} from "../store/modules/auth.js"
router.beforeEach(async (to, from, next) => {
  if(!auth.state.isAuthenticated)
    if(auth.state.user.expirationTime>Date.now())
    await store.dispatch('auth/currentUser')
  else{
      if(!auth.state.user.expirationTime>Date.now()){

      }
  }
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (!auth.state.isAuthenticated) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    } else {

      if(to.matched.some(record => record.meta.isAdmin)) {
        if(auth.state.user.power=="ROLE_ADMIN,"){

          next()
        }
        else{
          next({ name: 'home'})
        }
      }else {
        next()
      }
    }
  }

  else if(to.matched.some(record => record.meta.guest)) {
    if(!auth.state.isAuthenticated){
      next()
    }
    else{
      next({ name: 'home'})
    }
  }else {
    next()
  }
})

export default router
