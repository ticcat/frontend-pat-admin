module.exports = {
  pwa: {
    name: 'Plataforma CCAT',
    themeColor: '#1976D2',
    msTileColor: '#82B1FF',
    manifestOptions: {
      background_color: '#ffffff',
      version: 1.2

    },

    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: '#ffffff',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js',
      exclude: [
        /\.map$/,
        /manifest\.json$/
      ],
    },
  },
  "transpileDependencies": [
    "vuetify"
  ],
  // options...
  devServer: {
    port: 8080,

    proxy: {
      '/api': {
        target: 'http://localhost:8088/api',
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
        },

      }
    }
  },
  chainWebpack: (config) => {
    config
        .plugin('html')
        .tap((args) => {
          args[0].title = 'Plataforma CCAT';
          return args;
        });
  },
  outputDir: '../server/src/main/resources/static'
}
